## Python Inventory system

## Description
This application is currently still work in progress, however end goal will be a collection of apis to automate the task of scanning a barcode, looking up the upc against an external database, recording item found, and giving local (and remote) access via a web server.

## Installation
Current installation method is via docker-compose.

```
docker-compose up
```

each folder has it's own set of requirement files now for each api

## Usage
I've migrated to docker for faster development prior to finishing the alpha so usage is customized via the docker-compose file

You'll still need to use the scan.py file locally still debating containerizing that as well

```
./scan.py
```

## Roadmap
will list after alpha is complete

## Authors and acknowledgment
Huge thank you to @superfly for your awesome input, really appreciate your insite on refactoring the base design

Also want to thank piddlerontheroof for the intial idea: https://www.instructables.com/USB-Barcode-Scanner-Raspberry-Pi/

and Binary101010 for reiterating (superfly said it first but didn't get it first go around) on this reddit thread what "bar code scanners are like usb keyboards" means: https://www.reddit.com/r/learnpython/comments/bxzqay/using_usb_barcode_scanner_in_python/

## Project status
In progress

