#!/usr/bin/python
import requests
import json
        
def barcode_reader():
    while True:
        barcode = input("Scanning:")
        response = requests.request("POST",  f"http://web-app:8080/{barcode}")
    return json.dumps(response.json())

barcode_reader()
