from flask import Flask, render_template, request, url_for, flash, redirect
import requests
import json
from werkzeug.exceptions import abort


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secretsprecioussecrets'

def item_lookup(barcode):
    requests.request("GET",  f"http://db-read:8080/{item_barcode}")

@app.route('/')
def index():
    inventory = requests.request("GET",  f"http://db-read-all:8080/")
    return render_template('index.html', items=inventory.json())

@app.route('/<int:item_barcode>')
def item(item_barcode):
    item = requests.request("GET",  f"http://db-read:8080/{item_barcode}")

    if item == None:
        print("item not found locally")
        query_upc_database = requests.request("POST",  f"http://upc-reader:8080/{barcode}")
        return query_upc_database
    else:
        return render_template('item.html', item=item.json())

@app.route('/add_item', methods=('GET', 'POST'))
def add_item():
    if request.method == 'POST':
        title = request.form['title']
        quantity = request.form['quantity']
        description = request.form['description']
        brand = request.form['brand']
        category = request.form['category']
        barcode = request.form['barcode']

        if not title:
            flash('Title required!')
        elif not quantity:
            flash('Quantity required!')
        
        else:
            requests.request("POST",  f"http://db-add-item:8080/{barcode}/{title}/{description}/{quantity}/{brand}/{category}")        
            return redirect(url_for('index'))
    
    return render_template('add-item.html') #, item=item.json())

@app.route('/delete_item')
def delete_item():
    #item = requests.request("GET",  f"http://db-delete-item:8080/{item_barcode}")
    return render_template('delete-item.html')

@app.route('/update_quantity')
def update_quantity():
    #item = requests.request("GET",  f"http://db-update-quantity:8080/{item_barcode}")
    return render_template('update-quantity.html')
