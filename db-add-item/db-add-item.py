from typing import Optional
from fastapi import FastAPI
import requests
from sqlmodel import Field, Session, SQLModel, create_engine

app = FastAPI()

class Food(SQLModel, table=True):
    barcode: int = Field(primary_key=True)
    title: str
    description: Optional[str] = None
    quantity: int
    brand: Optional[str] = None
    category: Optional[str] = None

postgres_db = "postgresql://postgres:password1@db:5432/scanner"
engine = create_engine(postgres_db)

SQLModel.metadata.create_all(engine)

@app.post("/{barcode}/{title}/{description}/{quantity}/{brand}/{category}")
def read_item(barcode: int, title: str, description: Optional[str], quantity: int, brand: Optional[str], category: Optional[str]):
    food = Food(barcode=barcode, title=title, description=description, quantity=quantity, brand=brand, category=category)
    with Session(engine) as session:
        session.add(food)
        session.commit()
        return food
