fastapi==0.70.0
sqlmodel==0.0.5
psycopg2-binary==2.9.2
requests==2.26.0
uvicorn==0.15.0
