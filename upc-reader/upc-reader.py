from typing import Optional
import requests
import json
from fastapi import FastAPI

app = FastAPI()
api_key = ""
url = "https://api.upcdatabase.org/product/"
headers = { 'cache-control': "no-cache", }

@app.post("/{barcode}")
def read_item(barcode: int):
    url_parsed = f"{url}{barcode}?apikey={api_key}"
    response = requests.request("GET", url_parsed, headers=headers)
    return json.dumps(response.json())