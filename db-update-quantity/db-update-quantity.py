from typing import Optional
from fastapi import FastAPI
import requests
import json
from sqlmodel import Field, Session, SQLModel, create_engine, select

app = FastAPI()

class Food(SQLModel, table=True):
    #id: int 
    barcode: int = Field(primary_key=True)
    title: str
    description: Optional[str] = None
    quantity: int
    brand: Optional[str] = None
    category: Optional[str] = None

postgres_db = "postgresql://postgres:password1@db:5432/scanner"
engine = create_engine(postgres_db)

SQLModel.metadata.create_all(engine)

@app.put("/add/{barcode}/{quantity}")
def read_item(barcode: int, quantity: int):
    with Session(engine) as session:
        food = session.get(Food, barcode)
        food.quantity = quantity
        session.add(food)
        session.commit()
        session.refresh(food)
        return food
