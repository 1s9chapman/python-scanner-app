from typing import Optional
#from sqlalchemy.sql.expression import select
from fastapi import FastAPI
from sqlmodel import Field, Session, SQLModel, create_engine, select

app = FastAPI()

class Food(SQLModel, table=True):
    #id: int 
    barcode: int = Field(primary_key=True)
    title: str
    description: Optional[str] = None
    quantity: int
    brand: Optional[str] = None
    category: Optional[str] = None

postgres_db = "postgresql://postgres:password1@db:5432/scanner"
engine = create_engine(postgres_db)

SQLModel.metadata.create_all(engine)

@app.get("/{barcode}")
def read_item(barcode: int):
    with Session(engine) as session:
        # since barcode is the primary key we can use the get function rather than the three lines below
        food = session.get(Food, barcode)
        #statement = select(Food).where(Food.barcode == barcode)
        #results = session.exec(statement)
        #food = results.first()
        return food
