python-sql==0.9
psycopg2-binary==2.9.2
sqlmodel==0.0.5
fastapi==0.70.0
uvicorn==0.15.0
