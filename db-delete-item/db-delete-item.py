from typing import Optional
from fastapi import FastAPI
import requests
from sqlmodel import Field, Session, SQLModel, create_engine, select

app = FastAPI()

class Food(SQLModel, table=True):
    #id: int 
    barcode: int = Field(primary_key=True)
    title: str
    description: Optional[str] = None
    quantity: int
    brand: Optional[str] = None
    category: Optional[str] = None

postgres_db = "postgresql://postgres:password1@db:5432/scanner"
engine = create_engine(postgres_db)

SQLModel.metadata.create_all(engine)

@app.put("/test/{barcode}")
def read_item(barcode: str):
    with Session(engine) as session:
        food = session.get(Food, barcode)
        session.delete(food)
        session.commit()
        return food
